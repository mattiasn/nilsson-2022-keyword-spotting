def mi_sorted(R, num_r,
              S_size, S_trials):
    """Calculate mutual information for data sorted by input, S.

    Args:
        R (list/array):     Response data
        num_r (int):        Number of variables in R
        S_size (int):       Number of possible discrete values of S
        S_trials (in):      Number of trials for each of the possible values of S

    Returns:
        avg (int):  Average resulting mutual information
        std (int):  Standard deviation of mutual information results
    """
    # Max value of R
    R_max = max(R) + 1

    # Dimensions of R
    R_dims = (num_r, R_max)

    # Results array
    mi = np.array([])

    # Create sorted discrete system
    ds = pe.SortedDiscreteSystem(R, R_dims,
                                 S_size, S_trials)

    # Calculate entropies
    ds.calculate_entropies(method ='plugin', calc=['HX', 'HXY'])
    mi = np.append(mi, ds.I())
    
    ds.calculate_entropies(method = 'pt', calc=['HX', 'HXY'])
    mi = np.append(mi, ds.I())
    
    ds.calculate_entropies(method='qe', calc=['HX', 'HXY'])
    mi = np.append(mi, ds.I())

    # Get average value and standard deviation
    avg = np.average(mi)
    std = np.std(mi)
    
    return avg, std


def mi(R, num_r,
       S, num_s):
    
    # Max value of R
    R_max = max(R) + 1
    S_max = max(S) + 1

    # Dimensions of R
    R_dims = (num_r, R_max)
    S_dims = (num_s, S_max)

    # Results array
    mi = np.array([])

    # Create sorted discrete system
    ds = pe.DiscreteSystem(R, R_dims,
                           S, S_dims)

    # Calculate entropies
    ds.calculate_entropies(method='plugin', calc=['HX', 'HXY'])
    mi = np.append(mi, ds.I())
    
    ds.calculate_entropies(method='pt', calc=['HX', 'HXY'])
    mi = np.append(mi, ds.I())
    
    ds.calculate_entropies(method='qe', calc=['HX', 'HXY'])
    mi = np.append(mi, ds.I())

    # Get average and standard deviation
    avg = np.average(mi)
    std = np.std(mi)
    
    return avg, std


def get_test_mi(num_words,
                selection_factors,
                dirname_in):
    """Calculate mutual-information values for network test-results.

    Args:
        num_words (_type_): _description_
        selection_factors (_type_): _description_
        dirname_in (_type_): _description_

    Returns:
        _type_: _description_
    """
    # Numbers of response variables
    num_R_fr = 1
    num_R_ei = 1

    # Number of possible discrete values of S (stimulus space)
    S_size = 2

    # MI result arrays
    mis_fr = np.array([])
    stds_fr = np.array([])

    mis_ei = np.array([])
    stds_ei = np.array([])

    # Loop over selection factors
    for selection_factor in selection_factors:

        R_fr, R_ei, num_sampl = pruned_counts(dirname_in,
                                             num_words,
                                             selection_factor)
        
        # Number of trials per value of S
        S_trials = np.array([num_sampl,
                             num_sampl + 1])
    
        # Get MI for formant-neurons
        mi_fr, std_fr = mi_sorted(R_fr, num_R_fr,
                                  S_size, S_trials)
        
        # Store results
        mis_fr = np.append(mis_fr, mi_fr)
        stds_fr = np.append(stds_fr, std_fr)


        # Get MI for EI-neurons
        mi_ei, std_ei = mi_sorted(R_ei, num_R_ei,
                                  S_size, S_trials)
        
        # Store results
        mis_ei = np.append(mis_ei, mi_ei)
        stds_ei = np.append(stds_ei, std_ei)
        
    return(mis_fr, stds_fr,
           mis_ei, stds_ei)