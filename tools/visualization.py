def countplot_training(traincount_fr,
                       traincount_ei):
    
    num_fr = len(traincount_fr)
    num_ei = len(traincount_ei)
    
    # Rank-order training spike-counts
    countrank_fr = np.argsort(traincount_fr)
    countrank_ei = np.argsort(traincount_ei)

    # Create figure 
    fig, axs = plt.subplots(2, 2, figsize = (15,15))

    # Formant spike-counts
    axs[0,0].bar(range(num_fr), traincount_fr)
    axs[0,0].set_title("Accumulated formant spike-counts")
    axs[0,0].set_xlabel("Channel")
    axs[0,0].set_ylabel("Spike-count")

    # Formant spike-counts (sorted)
    axs[0,1].bar(range(num_fr), traincount_fr[countrank_fr])
    axs[0,1].set_title("Accumulated formant spike-counts (sorted)")
    axs[0,1].set_xlabel("Channel")
    axs[0,1].set_ylabel("Spike-count")

    # E-I spike-counts
    axs[1,0].bar(range(num_ei), traincount_ei)
    axs[1,0].set_title("Accumulated E-I spike-counts")
    axs[1,0].set_xlabel("E-I neuron")
    axs[1,0].set_ylabel("Spike-count")

    # E-I spike-counts (sorted)
    axs[1,1].bar(range(num_ei), traincount_ei[countrank_ei])
    axs[1,1].set_title("Accumulated E-I spike-counts (sorted)")
    axs[1,1].set_xlabel("E-I neuron")
    axs[1,1].set_ylabel("Spike-count")


def barplot_test(dirname_in,
                 num_words,
                 selection_factor_fr,
                 selection_factor_ei):
    
    test_words = ['one', 'two', 'three', 'four']
    
    # Load test-results
    testcounts_fr = pickle.load(open(dirname_in + '/testcounts_fr.pkl', 'rb'))
    testcounts_ei = pickle.load(open(dirname_in + '/testcounts_ei.pkl', 'rb'))

    num_sampl = int((testcounts_fr.shape)[0] / num_words)
    
    counts_fr = dict()
    counts_ei = dict()
    
    # Loop over out-class words
    for i in range(0, num_words):
        
        # Sampling indices
        start = i * num_sampl
        stop = start + num_sampl
        
        # Get and accumulate counts
        counts_fr[i] = np.sum(testcounts_fr[start:stop,:], axis=0).astype(int)
        counts_ei[i] = np.sum(testcounts_ei[start:stop,:], axis=0).astype(int)
    
        ind_fr, dummy = prune(dirname_in, num_words, selection_factor_fr)
        dummy, ind_ei = prune(dirname_in, num_words, selection_factor_ei)
    
    # Plot formant spike-counts
    fig, axs = plt.subplots(2, 2, figsize = (12,12))
    for i, ax in enumerate(fig.axes):
        
        ax.bar(range(len(counts_fr[i][ind_fr])), counts_fr[i][ind_fr])
        ax.set_ylim(0, 15e3)
        ax.set_title('"' + test_words[i] + '"')
        ax.set_xlabel("Formant channel")
        ax.set_ylabel("Spike-count")
        
    # Plot EI spike-counts
    fig, axs = plt.subplots(2, 2, figsize = (12,12))
    for i, ax in enumerate(fig.axes):
        
        ax.bar(range(len(counts_ei[i][ind_ei])), counts_ei[i][ind_ei])
        ax.set_ylim(0, 13e3)
        ax.set_title('"' + test_words[i] + '"')
        ax.set_xlabel("E-I neuron")
        ax.set_ylabel("Spike-count")
        
        
def errorbar_test(dirname_in,
                  num_words,
                  selection_factor_fr,
                  selection_factor_ei):
    
    test_words = ['one',
                  'two',
                  'three',
                  'four']
    
    # Load test-results
    testcounts_fr = pickle.load(open(dirname_in + '/testcounts_fr.pkl', 'rb'))
    testcounts_ei = pickle.load(open(dirname_in + '/testcounts_ei.pkl', 'rb'))

    num_sampl = int((testcounts_fr.shape)[0] / num_words)
    
    mean_fr = {}
    mean_ei = {}
    
    std_fr = {}
    std_ei = {}
    
    # Loop over out-class words
    for i in range(0, num_words):
        
        # Sampling indices
        start = i * num_sampl
        stop = start + num_sampl
        
        mean_fr[i] = np.mean(testcounts_fr[start:stop,:], axis=0)
        mean_ei[i] = np.mean(testcounts_ei[start:stop,:], axis=0)
        
        std_fr[i] = np.std(testcounts_fr[start:stop,:], axis=0)
        std_ei[i] = np.std(testcounts_ei[start:stop,:], axis=0)
        
    ind_fr, dummy = prune(dirname_in, num_words, selection_factor_fr)
    dummy, ind_ei = prune(dirname_in, num_words, selection_factor_ei)
    
    ind_fr = np.sort(ind_fr)
    ind_ei = np.sort(ind_ei)
    
    # Plot formant spike-counts
    fig_fr, axs = plt.subplots(2, 2, figsize = (12,12))
    for i, ax in enumerate(fig_fr.axes):
        
        # Sampling indices
        start = i * num_sampl
        stop = start + num_sampl
        
        ax.errorbar(range(len((mean_fr[i]))),
                    mean_fr[i],
                    std_fr[i])
        
        ax.errorbar(ind_fr,
                    mean_fr[i][ind_fr],
                    std_fr[i][ind_fr])
        
        ax.set_ylim(-10, 120)
        ax.set_title('"' + test_words[i] + '"')
        ax.set_xlabel("Formant channel")
        ax.set_ylabel("Spike-count")
        ax.legend(["All channels",
                   str(selection_factor_fr*1e2) + '%'])
        
    # Plot EI spike-counts
    fig_ei, axs = plt.subplots(2, 2,
                               figsize = (12,12))
    for i, ax in enumerate(fig_ei.axes):
        
        # Sampling indices
        start = i * num_sampl
        stop = start + num_sampl
        
        ax.errorbar(range(len((mean_ei[i]))),
                    mean_ei[i],
                    std_ei[i])
        
        ax.errorbar(ind_ei,
                    mean_ei[i][ind_ei],
                    std_ei[i][ind_ei])
        
        ax.set_ylim(-5, 95)
        ax.set_title('"' + test_words[i] + '"')
        ax.set_xlabel("E-I neuron")
        ax.set_ylabel("Spike-count")
        ax.legend(["All neurons",
                   str(selection_factor_ei*1e2) + '%'])
        
    plt.savefig("Images/errorbar_ei.pdf")