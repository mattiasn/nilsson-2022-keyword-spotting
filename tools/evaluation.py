import numpy as np

from sklearn.metrics import accuracy_score, auc, roc_curve


def subsample(spikecounts,
              keyword_index,
              num_words):

    # Get number of neurons
    num_neur = (spikecounts.shape)[0]

    # Total number of samples
    num_sampl = (spikecounts.shape)[1]
    
    # Number of samples per word
    num_sampl_word = int(num_sampl / num_words)
    
    # Subsample size
    num_sub = round(num_sampl_word/(num_words-1))
    
    # Total number of output samples
    num_tot = num_sampl_word + num_sub*(num_words-1)
    
    # Matrices for subsampled test-data
    counts_sub = np.zeros((num_neur, num_tot))

    # Data-writing index
    start_w = 0
    
    # Initialize random-number generator
    # - NOTE: Ensure that subsampling is identical for comparative evaluations
    rng = np.random.default_rng(42)
    
    # Loop over words
    for i in range(num_words):
        
        # Data-reading indices    
        start_r = i * num_sampl_word
        stop_r = start_r + num_sampl_word
        
        r_inds = np.arange(start_r, stop_r)
        
        # Set number of sample entries
        if i == keyword_index:
            num_entry = num_sampl_word
            
        else:
            num_entry = num_sub
            
            # Randomize indices for subsampling
            rng.shuffle(r_inds)
        
        # Set data-writing indices
        stop_w = start_w + num_entry
        
        # Store keyword sample-indices
        if i == keyword_index:
            start_kw = start_w
            stop_kw = stop_w
        
        # Read and write samples
        counts_sub[:, start_w:stop_w] = spikecounts[:, r_inds[:num_entry]]
        
        # Update writing index
        start_w = stop_w
    
    # In-class sample indices
    inds_inclass = np.arange(start_kw, stop_kw)
    
    # Out-class sample indices
    inds_outclass = np.arange(start_kw)
    inds_outclass = np.append(inds_outclass, np.arange(stop_kw, num_tot))
    
    return counts_sub, inds_inclass, inds_outclass


def rank_neurons(keyword_index,
                 dirnames_in,
                 num_words,
                 method):
    """Rank-order neurons for pruning based on training data.
    """
    # Loop over result sets
    for i, dirname in enumerate(dirnames_in):
    
        # Load data
        if i is 0:
            traincounts = np.loadtxt(dirname + 'train/counts.csv', delimiter=',')
        else: 
            traincounts = np.append(traincounts,
                                    np.loadtxt(dirname + 'train/counts.csv', delimiter=','),
                                    axis = 0)

    # Subsample data
    counts_sub, inds_in, inds_out = subsample(traincounts,
                                              keyword_index,
                                              num_words) 
    
    # Get in-class spike-counts
    count_in = np.sum(counts_sub[:,inds_in], axis=1).astype(int)
    
    # Get out-class spike-counts
    count_out = np.sum(counts_sub[:,inds_out], axis=1).astype(int)

    # Rank-order neurons
    if method is 'ratio':
        # Add artificial spike to avoid division with zero
        count_out[np.argwhere(count_out == 0)] = 1
        
        # Rank by spike-count ratio
        ind_ranked = np.argsort(count_in / count_out)
    
    elif method is 'difference':
        ind_ranked = np.argsort(count_in - count_out)
    
    elif method is 'max_inclass':
        ind_ranked = np.argsort(count_in)
    
    else:
        print("Error: No pruning method selected.")
        return

    return ind_ranked
    
    
def pruned_counts(dirnames_in,
                  keyword_index,
                  ind_ranked,
                  num_words,
                  num_pruned):
    """Get spike-counts of pruned populations for test-data.
    """
    # Loop over result sets
    for i, dirname in enumerate(dirnames_in):
    
        # Load data
        if i is 0:
            testcounts = np.loadtxt(dirname + 'test/counts.csv', delimiter=',')
        else:
            testcounts = np.append(testcounts,
                                   np.loadtxt(dirname + 'test/counts.csv', delimiter=','),
                                   axis = 0)

    # Matrices for subsampled test-data
    counts_sub, inds_inclass, inds_outclass = subsample(testcounts,
                                                        keyword_index,
                                                        num_words)

    # Get indices of top-ranking neurons
    ind_pruned = ind_ranked[-num_pruned:]    

    # Get sum counts of pruned population
    counts = np.sum(counts_sub[ind_pruned,:], axis=0).astype(int)
    
    return counts, inds_inclass


def roc(keyword_index,
        num_words,
        num_pruned,
        dirnames_in,
        pruning_method):
    """Get ROC and accuracy metrics.
    """
    # Result variables
    fpr = dict()
    tpr = dict()
    thresholds = dict()
    acc = np.zeros(len(num_pruned))
    roc_auc = np.array([])
    
    # Get rank-ordered neuron indices
    ind_ranked = rank_neurons(keyword_index,
                              dirnames_in,
                              num_words,
                              method = pruning_method)
    
    # Loop over selection factors
    for i, num_pr in enumerate(num_pruned):
    
        # Get spike-counts of pruned population
        scores, inds_keyword = pruned_counts(dirnames_in,
                                             keyword_index,
                                             ind_ranked,
                                             num_words,
                                             num_pr)
        
        # Ground truth for binary classification
        y_true = np.zeros_like(scores)
        y_true[inds_keyword] = 1

        # Get ROC curve
        fpr[i], tpr[i], thresholds[i] = roc_curve(y_true,
                                                  scores,
                                                  drop_intermediate = False)
        
        # Get ROC area-under-curve
        #roc_auc = np.append(roc_auc, auc(fpr[i], tpr[i]))
        
        # Threshold-specific accuracies
        acc_thr = np.array([])
            
        # Loop over thresholds
        for thr in thresholds[i]:
            
            # Get accuracy
            acc_thr = np.append(acc_thr,
                                accuracy_score(y_true, [m > thr for m in scores]))
            
        # Store best accuracy for selection factor
        acc[i] = acc_thr.max()
        
        # Get threshold for max accuracy
        #threshold_max_acc_fr =  thresholds_fr[acc_fr.argmax()]
        
    return fpr, tpr, thresholds, acc


def count_per_sample(feature_ind,
                     keyword_index,
                     n_words,
                     dirname):
    """Get mean spike-count per keyword sample for specific neuron.
    """
    # Get spike-counts (n_features, n_samples)
    spikecounts = np.loadtxt(dirname + 'test/counts.csv', delimiter=',')

    # Get number of samples
    n_sampl = spikecounts.shape[1]
    n_sampl_per_word = int(n_sampl / n_words)
    
    # Sample indices
    start = keyword_index * n_sampl_per_word
    stop = start + n_sampl_per_word
    
    # Get mean spike-count per sample
    mean = np.mean(spikecounts[feature_ind, start:stop])
    std = np.std(spikecounts[feature_ind, start:stop])

    return mean, std