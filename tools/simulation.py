import numpy as np
import os
import scipy.io as sio
from tqdm.notebook import tqdm

import tools.data_handling as dh

from brian2 import ms, second


def run_layer1(filepath_in,
               dirpath_out,
               stage,
               digits,
               num_samples,
               network,
               spikegen,
               num_fr,
               num_out,
               store_spikes=True,
               store_counts=True):

    t_append = 100 * ms
    
    spikemon_out = network.spikemonitors['spikemon_out']
    
    # Load input data
    indata = sio.loadmat(filepath_in, simplify_cells=True)

    dt_samp = indata['dt_samp']

    # Randomly shuffle file indices
    #rng = np.random.default_rng(data_seed)
    #rng.shuffle(filepath)

    # Spike-count arrays
    counts_fr = np.zeros((num_fr, num_samples))
    counts_out = np.zeros((num_out, num_samples))
        
    # Trial counter
    trial_count = 0

    # Loop over digits
    for digit in tqdm(digits, desc='Digits'):
        
        # Get digit data
        digit_data = indata[digit]

        # Get number of digit samples
        num_sampl = digit_data.shape[0]

        # Loop over samples
        for sample in tqdm(range(num_sampl), desc='Digit samples'):
            
            # Reset network state
            network.restore('initialized')
            
            # Get input spike-data
            spike_data = digit_data[sample]
            
            # Generate input spike-train
            i_in, t_in = dh.spike_train(spike_data, dt_samp)
            
            # Configure spike-generator
            spikegen.set_spikes(i_in, t_in*second, sorted=True)
            
            # Set duration and run simulation
            t_dur = t_in[-1]*second + t_append
            network.run(t_dur)
            
            # Store output spike-data
            i_out = spikemon_out.i
            t_out = spikemon_out.t / second
            
            # Count input spikes
            for i in range(num_fr):
                counts_fr[i, trial_count] = np.count_nonzero(i_in == i)
            
            # Get output spike-count
            counts_out[:,trial_count] = spikemon_out.count
                
            trial_count += 1
            
            # Store spike-data
            if store_spikes:
                np.savetxt(dirpath_out + stage + '/' + digit + '/i_out_' + str(sample) + '.csv',
                           i_out,
                           delimiter = ',')
                
                np.savetxt(dirpath_out + stage + '/' + digit + '/t_out_' + str(sample) + '.csv',
                           t_out,
                           delimiter = ',')

    # Store spike-counts
    if store_counts:
        np.savetxt(dirpath_out + stage + '/counts_fr.csv', counts_fr, delimiter=',')
        np.savetxt(dirpath_out + stage + '/counts_out.csv', counts_out, delimiter=',')
        

def run_layer2(filepath_in,
               dirpath_out,
               stage,
               digits,
               num_samples,
               network,
               spikegen,
               num_out,
               store_counts = True,
               store_spikes = True):
    
    t_append = 100 * ms
    
    spikemon_out = network.spikemonitors['spikemon']

    # Spike-count array
    counts_out = np.zeros((num_out, num_samples))
        
    # Trial counter
    trial_count = 0

    # Loop over digits
    for digit in tqdm(digits, desc='Digits'):
        
        digit_files = os.listdir(filepath_in + digit + '/')

        # Get number of digit samples
        num_sampl = int(len(digit_files) / 2)

        # Loop over samples
        for sample in tqdm(range(num_sampl), desc='Digit samples'):
            
            # Reset network state
            network.restore('initialized')
            
            # Load input spike-train
            i_in = np.loadtxt(filepath_in + digit + '/i_out_' + str(sample) + '.csv',
                              delimiter = ',')
            t_in = np.loadtxt(filepath_in + digit + '/t_out_' + str(sample) + '.csv',
                              delimiter = ',')
            
            # Configure spike-generator
            spikegen.set_spikes(i_in, t_in*second, sorted=True)
            
            # Set duration and run simulation
            t_dur = t_in[-1]*second + t_append
            network.run(t_dur)
            
            # Store output spike-data
            i_out = spikemon_out.i
            t_out = spikemon_out.t / second
            
            # Get output spike-count
            counts_out[:,trial_count] = spikemon_out.count
                
            trial_count += 1
            
            # Store spike-data
            if store_spikes:
                np.savetxt(dirpath_out + stage + '/' + digit + '/i_out_' + str(sample) + '.csv',
                           i_out,
                           delimiter = ',')
                
                np.savetxt(dirpath_out + stage + '/' + digit + '/t_out_' + str(sample) + '.csv',
                           t_out,
                           delimiter = ',')

    # Store spike-counts
    if store_counts:
        np.savetxt(dirpath_out + stage + '/counts_out.csv', counts_out, delimiter=',')