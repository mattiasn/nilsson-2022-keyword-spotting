import numpy as np
import pandas as pd
import pickle

from brian2 import ms


def spike_train(spike_data, dt):
    """Generate spike-train arrays from spike-sampling data.

    Args:
        spike_data:     Spike-sampling matrix
        dt:             Sampling period

    Returns:
        i:  Spike-index array
        t:  Spike-time array
    """
    
    # Get data dimensions
    n_ch, n_t = spike_data.shape

    # Get spike-data indices
    channels = range(n_ch)
    tsteps = range(n_t)

    # Create spike-train arrays
    i = np.array([])
    t = np.array([])

    # Loop over input-data timesteps (file rows)
    for tstep in tsteps:

        # Loop over input-data channels (file columns)
        for channel in channels: 

            # Get corresponding time
            time = tstep * dt

            # Check for spike
            if spike_data[channel, tstep] > 0:
                
                # Append spike-data
                i = np.append(i, channel)
                t = np.append(t, time)
                
    return i, t


def read_csv_file(filepath):
    """Read a file from dataset.

    Args:
        filepath (_type_): _description_

    Returns:
        _type_: _description_
    """
    # Timestep size
    dt = 1 * ms

    # Read spike-data
    spike_data = pd.read_csv(filepath)

    # Get data dimensions
    n_t, n_ch = spike_data.shape

    # Get spike-data indices
    timesteps = range(n_t)
    channels = range(n_ch)

    # Create spike-train arrays
    spike_inds = np.array([])
    spike_times = np.array([])

    # Loop over input-data timesteps (file rows)
    for timestep in timesteps:

        # Loop over input-data channels (file columns)
        for channel in channels: 

            # Get corresponding time
            time = timestep * dt

            # Check for spike
            if spike_data.iloc[timestep, channel] > 0:
                
                # Append spike-data
                spike_inds = np.append(spike_inds, channel)
                spike_times = np.append(spike_times, time)
                
    return spike_inds, spike_times