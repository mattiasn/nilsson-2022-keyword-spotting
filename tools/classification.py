import numpy as np
from sklearn import preprocessing
from sklearn.inspection import permutation_importance
from sklearn.metrics import f1_score

import tools.evaluation as ev


def train(model,
          feature_inds,
          keyword_index,
          num_words,
          dirnames_in,
          scale_data):
    """Train classifier.
    """
    # Loop over result sets
    for i, dirname in enumerate(dirnames_in):
    
        # Load data
        if i is 0:
            spikecounts = np.loadtxt(dirname + 'train/counts.csv', delimiter=',')
        else: 
            spikecounts = np.append(spikecounts,
                                    np.loadtxt(dirname + 'train/counts.csv', delimiter=','),
                                    axis = 0)
    
    # Subsample data for balancing
    spikecounts, inds_inclass, inds_outclass = ev.subsample(spikecounts,
                                                            keyword_index,
                                                            num_words)
    
    # Reshape data for classifier (n_samples, n_features)
    if feature_inds is 'all':
        X = np.transpose(spikecounts)
    else:
        # Select features
        X = np.transpose(spikecounts[feature_inds, :])
    
    # Scale data
    if scale_data:
        scaler = preprocessing.StandardScaler()
        scaler.fit(X)
        X = scaler.transform(X)
    
    # Define target values (n_samples, n_targets=1)
    y = np.zeros(X.shape[0])
    y[inds_inclass] = 1
    
    # Train model
    model.fit(X, y)
    
    # Get prediction score
    score = model.score(X, y)

    return score


def test(model,
         feature_inds,
         keyword_index,
         num_words,
         dirnames_in,
         scale_data):
    """Test classifier.
    """
    # Loop over result sets
    for i, dirname in enumerate(dirnames_in):
        
        # Load data
        if i is 0:
            spikecounts = np.loadtxt(dirname + 'test/counts.csv', delimiter=',')
        else: 
            spikecounts = np.append(spikecounts,
                                    np.loadtxt(dirname + 'test/counts.csv', delimiter=','),
                                    axis = 0)
    
    # Subsample data for balancing
    spikecounts, inds_inclass, inds_outclass = ev.subsample(spikecounts,
                                                            keyword_index,
                                                            num_words)
    
    # Reshape data for classifier (n_samples, n_features)
    if feature_inds is 'all':
        X = np.transpose(spikecounts)
    else:
        # Select features
        X = np.transpose(spikecounts[feature_inds, :])
        
    n_samples = X.shape[0]
    
    # Scale data
    if scale_data:
        scaler = preprocessing.StandardScaler()
        scaler.fit(X)
        X = scaler.transform(X)
    
    # Define target values (n_samples, n_targets)
    #   - NOTE: n_targets = 1
    y_true = np.zeros(X.shape[0])
    y_true[inds_inclass] = 1
    
    # Get prediction score
    score = model.score(X, y_true)
    
    y_pred = model.predict(X)  # (n_samples,)
    
    #f1 = f1_score(y_true, y_pred)
    
    n_tp = 0
    n_tn = 0
    
    tps = []
    tp_ind = 0
    
    # Loop over predictions
    for i, (yt, yp) in enumerate(zip(y_true, y_pred)):
        
        # In-class
        if yt == 1:
            
            # True positives
            if yp == 1:
                n_tp += 1
                tps.append(tp_ind)    
            
            # Update index
            tp_ind += 1
            
        if yt == 0 and yp == 0:
            n_tn += 1
    
    tpr = n_tp / n_samples
    tnr = n_tn / n_samples
    
    return score, tpr, tnr, tps


def feature_importance(model,
                       keyword_index,
                       num_words,
                       dirnames_in,
                       scale_data = False):
    """Get permutation feature importance.
    """
    # Loop over result sets
    for i, dirname in enumerate(dirnames_in):
        
        # Load data
        if i is 0:
            spikecounts = np.loadtxt(dirname + 'test/counts.csv', delimiter=',')
        else: 
            spikecounts = np.append(spikecounts,
                                    np.loadtxt(dirname + 'test/counts.csv', delimiter=','),
                                    axis = 0)
    
    # Subsample data for balancing
    spikecounts, inds_inclass, inds_outclass = ev.subsample(spikecounts,
                                                            keyword_index,
                                                            num_words)
    
    # Reshape data for classifier (n_samples, n_features)
    X = np.transpose(spikecounts)
    
    # Scale data
    if scale_data:
        scaler = preprocessing.StandardScaler()
        scaler.fit(X)
        X = scaler.transform(X)
    
    # Define target values (n_samples, n_targets)
    #   - NOTE: n_targets = 1
    y = np.zeros(X.shape[0])
    y[inds_inclass] = 1
    
    # Get prediction score
    importance = permutation_importance(model, X, y)
    
    return importance