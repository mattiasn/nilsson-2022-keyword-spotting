def _tau_current_conversion(tau_or_i,
                            c_mem,
                            u_t):
    
    #cp = 1.5 * 1e-12    # DPI capacitance (F)
    #ut = 25 * 1e-3      # Thermal voltage (V)
    k = 0.7
    
    # Check if input is a list
    if isinstance(tau_or_i, list):
        
        # Return list of converted values
        return [(c_mem*u_t) / (k*x) for x in tau_or_i]
    
    # Return single converted value
    return (c_mem*u_t) / (k*tau_or_i)


def get_tau(current,
            dpi_capacitance,
            thermal_voltage):
    """Get time-constant resulting from given value of corresponding current.
    """
    return _tau_current_conversion(current,
                                   dpi_capacitance,
                                   thermal_voltage)


def get_current(timeconstant,
                dpi_capacitance,
                thermal_voltage):
    """Get current value yielding given time-constant value.
    """
    return _tau_current_conversion(timeconstant,
                                   dpi_capacitance,
                                   thermal_voltage)