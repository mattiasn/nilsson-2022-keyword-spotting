import numpy as np
import pickle

from brian2 import ms
from brian2 import SpikeMonitor, StateMonitor, SpikeGeneratorGroup

from teili import TeiliNetwork
from teili.core.groups import Neurons, Connections
from teili.models.neuron_models import DPI
from teili.models.synapse_models import DPISyn


def set_ei_params(neuron_groups,
                  exc_synapse_groups,
                  inh_synapse_groups):

    # Load E-I neuron parameters
    with open('Parameters/params_ei.pkl', 'rb') as file_in:
        params_ei = pickle.load(file_in)
        for neur in neuron_groups: 
            neur.set_params(params_ei)

    # Load exc. synapse parameters
    with open('Parameters/params_e.pkl', 'rb') as file_in:
        params_e = pickle.load(file_in)
        for syn_e in exc_synapse_groups:
            syn_e.set_params(params_e)

    # Load inh. synapse parameters
    with open('Parameters/params_i.pkl', 'rb') as file_in:
        params_i = pickle.load(file_in)
        for syn_i in inh_synapse_groups:
            syn_i.set_params(params_i)


def l1_connections(n_unique,
                   num_dupl,
                   num_in):
    
    # Connection arrays
    i = []
    j = []

    target_addr = 0    

    # Loop over unique target neurons (excl. duplicates)
    for l1_ind in range(n_unique):
        
        # Loop over duplicates
        for dupl_ind in range(num_dupl):
            
            # Loop over source neurons
            for source_ind in range(num_in):
                
                # Get source-neuron address
                source_addr = l1_ind + source_ind 
                
                # Set connection data
                i.append(source_addr)
                j.append(target_addr) 
                
            # Update global target-neuron address
            target_addr += 1
    
    return i, j 
            
            
def l2_connections(n_unique,
                   num_dupl,
                   num_in):
    
    # Connection arrays
    i = []
    j = []

    target_addr = 0
            
    # Loop over unique target neurons (excl. duplicates)
    for l2_ind in range(n_unique):
        
        # Loop over duplicates
        for dupl_ind in range(num_dupl):
            
            # Loop over source neurons
            for source_ind in range(num_in):
                
                # Get source-neuron address
                source_addr = (l2_ind+source_ind) * num_dupl + dupl_ind
                
                # Set connection data
                i.append(source_addr)
                j.append(target_addr) 
                
            # Update global target-neuron address
            target_addr += 1
    
    return i, j


def ei_network(num_ch,
               num_dupl,
               d_max,
               mismatch_syn):
    
    # Topological parameters
    num_in = d_max + 1                      # Number of inputs per E-I neuron
    num_syn = 2 * num_in
    
    num_ei_unique = num_ch - num_in + 1     # Number of unique E-I neurons
    num_ei = num_dupl * num_ei_unique       # Total number of E-I neurons
    
    # Dummy spike-train arrays
    i_fr = np.asarray([0])
    t_fr = np.asarray([0]) * ms

    # Create spike-generator for input
    spikegen_fr = SpikeGeneratorGroup(num_ch,
                                      indices = i_fr,
                                      times = t_fr,
                                      name = 'spikegen_fr')

    # E-I neurons
    neur_ei = Neurons(N = num_ei,
                      equation_builder = DPI(
                          num_inputs = num_syn),
                      name = 'neur_ei')

    # Synapses
    syn_e = Connections(spikegen_fr, neur_ei, equation_builder=DPISyn(), name='syn_e')
    syn_i = Connections(spikegen_fr, neur_ei, equation_builder=DPISyn(), name='syn_i')

    # Get connection arrays
    i, j = l1_connections(num_ei_unique, num_dupl, num_in)
            
    # Realize connections
    syn_e.connect(i=i, j=j)
    syn_i.connect(i=i, j=j)
        
    # Set network parameters
    set_ei_params([neur_ei],
                  [syn_e],
                  [syn_i])
    
    # Activate mismatch
    syn_e.add_mismatch(mismatch_syn, seed=10)
    syn_i.add_mismatch(mismatch_syn, seed=11)
    
    # Set up spike-monitor
    spikemon_out = SpikeMonitor(neur_ei, name='spikemon_out')
    
    # Create Teili network
    network = TeiliNetwork()

    # Add network components
    network.add(spikegen_fr,
                neur_ei,
                syn_e, syn_i,
                spikemon_out)

    # Store initial network state
    network.store('initialized')
    
    # Return Brian network handles
    return network, spikegen_fr, spikemon_out


def ei_layer2(num_ch,
              num_dupl,
              d_max,
              mismatch_syn):
    
    # Topological parameters
    num_in = d_max + 1                          # Number of inputs per E-I neuron
    num_syn = 2 * num_in

    num_unique_l1 = num_ch - num_in + 1         # Numner of unique L1 neurons
    num_l1 = num_dupl * num_unique_l1           # Total number of L1 neurons

    num_unique_l2 = num_unique_l1 - num_in + 1  # Number of unique L2 neurons
    num_l2 = num_dupl * num_unique_l2           # Total number of L2 neurons

    # Dummy spike-train arrays
    i_l1 = np.asarray([0])
    t_l1 = np.asarray([0]) * ms

    # Create spike-generator for input
    spikegen = SpikeGeneratorGroup(num_l1,
                                   indices = i_l1,
                                   times = t_l1,
                                   name = 'spikegen_l1')
    
    # L2 neurons
    neur = Neurons(N = num_l2,
                   equation_builder = DPI(
                       num_inputs = num_syn),
                   name = 'neur')

    # L2 synapses
    syn_e = Connections(spikegen, neur, equation_builder=DPISyn(), name='syn_e')
    syn_i = Connections(spikegen, neur, equation_builder=DPISyn(), name='syn_i')
    
    # L2 connection arrays
    con_i, con_j = l2_connections(num_unique_l2, num_dupl, num_in)
            
    # Realize L2 connections
    syn_e.connect(i=con_i, j=con_j)
    syn_i.connect(i=con_i, j=con_j)
    
    # Set network parameters
    set_ei_params([neur],
                  [syn_e],
                  [syn_i])
    
    # Activate L2 mismatch
    syn_e.add_mismatch(mismatch_syn, seed=12)
    syn_i.add_mismatch(mismatch_syn, seed=13)
    
    # Set up spike-monitor
    spikemon = SpikeMonitor(neur, name='spikemon')
    
    # Create Teili network
    network = TeiliNetwork()

    # Add network components
    network.add(spikegen,
                neur,
                syn_e, syn_i,
                spikemon)

    # Store initial network state
    network.store('initialized')
    
    # Return Brian handles
    return network, spikegen, spikemon


def ei_twolayer_net(num_ch,
                    num_dupl,
                    d_max,
                    mismatch_syn):
    
    # Topological parameters
    num_in = d_max + 1                        # Number of inputs per E-I neuron
    num_syn = 2 * num_in

    num_unique_l1 = num_ch - num_in + 1           # Numner of unique L1 neurons
    num_l1 = num_dupl * num_unique_l1             # Total number of L1 neurons

    num_unique_l2 = num_unique_l1 - num_in + 1    # Number of unique L2 neurons
    num_l2 = num_dupl * num_unique_l2             # Total number of L2 neurons

    # Dummy spike-train arrays
    i_fr = np.asarray([0])
    t_fr = np.asarray([0]) * ms

    # Create spike-generator for input
    spikegen_fr = SpikeGeneratorGroup(num_ch,
                                      indices = i_fr,
                                      times = t_fr,
                                      name = 'spikegen_fr')
    
    """Layer 1
    """
    # L1 neurons
    neur_l1 = Neurons(N = num_l1,
                      equation_builder = DPI(
                          num_inputs = num_syn),
                      name = 'neur_l1')

    # L1 synapses
    syn_e_l1 = Connections(spikegen_fr, neur_l1, equation_builder=DPISyn(), name='syn_e_l1')
    syn_i_l1 = Connections(spikegen_fr, neur_l1, equation_builder=DPISyn(), name='syn_i_l1')

    # L1 connection arrays
    i_l1, j_l1 = l1_connections(num_unique_l1, num_dupl, num_in)
            
    # Realize L1 connections
    syn_e_l1.connect(i=i_l1, j=j_l1)
    syn_i_l1.connect(i=i_l1, j=j_l1)

    """Layer 2
    """
    # L2 neurons
    neur_l2 = Neurons(N = num_l2,
                      equation_builder = DPI(
                          num_inputs = num_syn),
                      name = 'neur_l2')

    # L2 synapses
    syn_e_l2 = Connections(neur_l1, neur_l2, equation_builder=DPISyn(), name='syn_e_l2')
    syn_i_l2 = Connections(neur_l1, neur_l2, equation_builder=DPISyn(), name='syn_i_l2')
    
    # L2 connection arrays
    i_l2, j_l2 = l2_connections(num_unique_l2, num_dupl, num_in)
            
    # Realize L2 connections
    syn_e_l2.connect(i=i_l2, j=j_l2)
    syn_i_l2.connect(i=i_l2, j=j_l2)
    
    # Set network parameters
    set_ei_params([neur_l1, neur_l2],
                  [syn_e_l1, syn_e_l2],
                  [syn_i_l1, syn_i_l2])
    
    # Activate L1 mismatch
    syn_e_l1.add_mismatch(mismatch_syn, seed=10)
    syn_i_l1.add_mismatch(mismatch_syn, seed=11)
    
    # Activate L2 mismatch
    syn_e_l2.add_mismatch(mismatch_syn, seed=12)
    syn_i_l2.add_mismatch(mismatch_syn, seed=13)
    
    # Set up spike-monitors
    spikemon_fr = SpikeMonitor(spikegen_fr, name='spikemon_fr')
    spikemon_out = SpikeMonitor(neur_l2, name='spikemon_out')
    
    # Create Teili network
    network = TeiliNetwork()

    # Add network components
    network.add(spikegen_fr,
                neur_l1,
                syn_e_l1, syn_i_l1,
                neur_l2,
                syn_e_l2, syn_i_l2,
                spikemon_fr, spikemon_out)

    # Store initial network state
    network.store('initialized')
    
    # Return Brian handles
    return network, spikegen_fr, spikemon_fr, spikemon_out