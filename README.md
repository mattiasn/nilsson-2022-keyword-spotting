# Nilsson 2022 Keyword Spotting

Code for parameter tuning and simulation of spiking neural networks with disynaptic excitatory–inhibitory (E–I) elements and classification of output data for the keyword spotting experiments presented in the paper "A Comparison of Temporal Encoders for Neuromorphic Keyword Spotting with Few Neurons" by Nilsson et al., 2022.

For questions about the simulations of time-difference encoders (TDEs), contact Ton Juny Pina (t.juny.pina@rug.nl).
